package demo.android.com.listviewdemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by aneeshjindal on 10/30/15.
 */
public class SubjectAdapter extends ArrayAdapter<SchoolSubject> {
    Context context;
    int resource;
    ArrayList<SchoolSubject> subjects = null;

    public SubjectAdapter(Context context, int resource, ArrayList<SchoolSubject> subjects) {
        super(context, resource, subjects);
        this.context = context;
        this.resource = resource;
        this.subjects = subjects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SchoolSubject subject = subjects.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(resource, parent, false);
        }

        TextView subjectNameTextView = (TextView)convertView.findViewById(R.id.subjectNameTextView);
        // TODO: 10/30/15 Finish this method. Use the line above as a guide (I was tempted to remove it, but you're welcome!)
    }
}
