package demo.android.com.listviewdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.row_view); // TODO: 10/30/15 Something is wrong with this line. Fix it.

        ListView listView = (ListView)findViewById(R.id.listView);

        ArrayList<SchoolSubject> subjects = new ArrayList<SchoolSubject>();

        // TODO: 10/30/15 Figure out what's missing. Hint, you need data to populate the List View with!

        listView.setAdapter(subjects); // TODO: 10/30/15 Why doesn't this line work?



        //Challenge!!! Do after doing the previous ones.
        // TODO: 10/30/15 CHALLENGE! Open a new activity that shows that subject's information when a row is clicked.
        //Hint for the challenge: use listView.setOnItemClickListener, as shown below:

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            //Think about the difference between this an a traditional onClickListener
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) { //This function is called whenever an item is clicked.
                //What do you have to do to open a new activity? And how do you pass the information to that new activity?
            }
        });

    }
}
